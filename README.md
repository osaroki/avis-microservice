<a href="https://satisficing.cloud">
    <img src="" alt="Logo Satisficing" title="Satisficing" align="right" height="50" />
</a>

# Microservice 'Avis' pour Satisficing

- Site : [satisficing.cloud](https://satisficing.cloud)
- Tutoriels : [learn.satisficing.cloud](https://learn.satisficing.cloud/)

Le microservice 'Avis' permet aux utilisateurs de noter et laisser des commentaires via un lien unique générer préalablement

## Ressources utilisées: 


[Deploy your first Flask+MongoDB app on Kubernetes](https://levelup.gitconnected.com/deploy-your-first-flask-mongodb-app-on-kubernetes-8f5a33fa43b4)
[Python Flask MongoDB - Complete CRUD in one video](https://www.youtube.com/watch?v=o8jK5enu4L4)


