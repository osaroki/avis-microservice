from flask import Flask, request, jsonify
from flask_pymongo import PyMongo
from bson.objectid import ObjectId
from datetime import datetime
import socket

app = Flask(__name__)

# Connexion avec la base de donnees mongo
try:
  app.config["MONGO_URI"] = "mongodb://avis.bdd:27017/avis"
  mongo = PyMongo(app)
  db = mongo.db
except:
  print("Erreur - Impossible d'etablir une connexion avec la base de donnees")



# Route principale permettant d'obtenir le hostname du pod
@app.get("/")
def index():
    try:
        hostname = socket.gethostname()
        return jsonify(
            message="API 'Avis', actuellement dans le pod {}".format(hostname)
        )
    except:
        print("Une erreur est survenue")

# Route permettant de recuperer la liste de tous les avis
@app.get("/liste_avis")
def tous_les_avis():
    try:
        tous_les_avis = db.avis.find()
        liste_avis = []
        for avis in tous_les_avis:
            item = {
                "id": str(avis["_id"]),
                "id_utilisateur": avis["id_utilisateur"],
                "id_module": avis["id_module"],
                "note": avis["note"],
                "date_avis": avis["date_avis"],
                "commentaire": avis["commentaire"]
            }
            liste_avis.append(item)
        return jsonify(
            liste_avis=liste_avis
        )
    except:
        return jsonify(
            message="Une erreur est survenue lors de la recuperation de la liste des avis"
        )

# Route permettant de creer un avis
@app.post("/avis")
def creer_avis():
    try:
        timestamp = datetime.now()
        data = request.get_json(force=True)
        db.avis.insert_one({
            "id_utilisateur": data["id_utilisateur"],
            "id_module": data["id_module"],
            "date_avis": timestamp.strftime("%d/%m/%Y %H:%M:%S"),
            "note": data["note"],
            "commentaire": data["commentaire"]
        })
        return jsonify(
            message="Avis creer avec succes !"
        )
    except:
        return jsonify(
            message="Une erreur est survenue lors de la creation de l'avis"
        )

# Route permettant de modifier un avis ( Specifier une note et un commentaire )
@app.put("/avis/<id>")
def modifier_avis(id):
    try:
        data = request.get_json(force=True)
        response = db.avis.update_one({
            "_id": ObjectId(id)
        }, {"$set": {
                "note": data["note"],
                "commentaire": data["commentaire"]
            }})
        if response.matched_count:
            message = "Avis modifier avec succes!"
        else:
            message = "L'avis {} n'existe pas".format(id)
        return jsonify(
            message=message
        )
    except:
        return jsonify(
          message="Une erreur est survenue lors de la modification de l'avis, syntaxe: /avis/<id>"
        )

# Route permettant de supprimer un avis
@app.delete("/avis/<id>")
def supprimer_avis(id):
      try:
        response = db.avis.delete_one({"_id": ObjectId(id)})
        if response.deleted_count:
            message = "Avis supprimer avec succes!"
        else:
            message = "L'avis {} n'existe pas".format(id)
        return jsonify(
            message=message
        ) 
      except:
          return jsonify(
            message="Une erreur est survenue lors de la suppression de l'avis, syntaxe: /avis/<id>"
          )

# Route permettant de supprimer tous les avis
@app.post("/avis/supprimer")
def supprimer_tous_les_avis():
    try:
        db.avis.remove()
        return jsonify(
            message="Tous les avis ont ete supprimes!"
        )
    except:
          return jsonify(
              message="Une erreur est survenue lors de la suppression de tous les avis"
          )

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
